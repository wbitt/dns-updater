# Update DNS with my WAN IP address:

There was a need to update some of my DNS names with the IP address of the WAN interface of my router. A lot of my friends also had this problem. Therefore I thought I should solve this itch once and for all.

This is useful/needed in situations where your server sits behind a router at your home, and you don't have a static IP, and the ISP keeps changing your public IP address.

The script will take a list of comma-separated DNS names through an environment variable, talk to the DNS provider using certain API and update it. 



