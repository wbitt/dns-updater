#!/bin/bash

# This program updates one or more DNS names with the IP address of our WAN IP address.

function do_update_digitalocean {
  # parameters: $1=FQDN_TO_BE_UPDATED, $2=something-else


  local FQDNS_TO_BE_UPDATED=$1  

  # Read digitalocean secrets from secrets file
  # *  account ID
  # *  secret Key

  if [ -z "${DIGITALOCEAN_TOKEN}" ]; then
    echo "The variable DIGITALOCEAN_TOKEN is empty. I won't be able to do anything without it."
    return
  fi

  echo "DIGITALOCEAN_TOKEN is found - $(echo ${DIGITALOCEAN_TOKEN} | cut -c 1-10) *****"
  ${DO_CLI} auth init --access-token ${DIGITALOCEAN_TOKEN}
  local DO_DOMAIN_COMMAND="${DO_CLI} compute domain records "

  for FQDN in $(echo ${FQDNS_TO_BE_UPDATED} | sed "s/,/\ /g") ; do

    echo "---------------------------------------------------------------------------------------------"

    echo "Processing ${FQDN} ..."

    FQDN_IP=$(check_fqdn_ip ${FQDN})
    
    if [ "${FQDN_IP}"  == "${MY_WAN_IP}" ]; then
      echo "${FQDN} IP is already equal to ${MY_WAN_IP} ... No action needed."
      continue
    else 
      echo "IP of ${FQDN} is ${FQDN_IP}, which is NOT equal to MY_WAN_IP: ${MY_WAN_IP}"
      echo "This script will fix this."
    fi

    DOMAIN_CHECK=$(do_check_if_fqdn_is_a_domain ${FQDN})

    # echo "DOMAIN_CHECK is received as: ${DOMAIN_CHECK}"
    
    if [ "${DOMAIN_CHECK}" == "domain" ]; then
      local RECORD_ID=$(${DO_DOMAIN_COMMAND} list ${FQDN} | grep -w 'A'| grep -w '@' | awk '{print $1}')
      if [ ! -z "${RECORD_ID}"  ]; then
      echo "${FQDN} is a 'domain'. Updating the 'A @' DNS record with ${MY_WAN_IP} ..."

        ${DO_DOMAIN_COMMAND} update \
          --record-id ${RECORD_ID} \
          --record-type A \
          --record-ttl 300 \
          --record-name @ \
          --record-data ${MY_WAN_IP} \
          ${FQDN}
      else
      echo "${FQDN} is a 'domain'. Creating the 'A @' DNS record with ${MY_WAN_IP} ..."

        ${DO_DOMAIN_COMMAND} create \
          --record-type A \
          --record-ttl 300 \
          --record-name @ \
          --record-data ${MY_WAN_IP} \
          ${FQDN}

      
      fi
    fi   

    if [ "${DOMAIN_CHECK}" == "host" ]; then
       echo "${FQDN} is a 'host' FQDN."

      local HOST_PART=$(echo ${FQDN} | cut -f1 -d\.)
      local DOMAIN_PART=$(echo ${FQDN} | sed "s/${HOST_PART}\.//g")
      
      local RECORD_ID=$(${DO_DOMAIN_COMMAND} list ${DOMAIN_PART} | grep -E -w ${HOST_PART} | awk '{print $1}')

      if [ -z "${RECORD_ID}"  ]; then
        echo "Record for FQDN ${FQDN} does not exist. Need to create it."

        ${DO_DOMAIN_COMMAND} create \
          --record-type A \
          --record-ttl 300 \
          --record-name ${HOST_PART} \
          --record-data ${MY_WAN_IP} \
          ${DOMAIN_PART}
      else
        echo "Record for FQDN ${FQDN} exists with RECORD_ID - ${RECORD_ID}."
        echo "Need to update this record."

        ${DO_DOMAIN_COMMAND} update \
          --record-id ${RECORD_ID} \
          --record-type A \
          --record-ttl 300 \
          --record-name ${HOST_PART} \
          --record-data ${MY_WAN_IP} \
          ${DOMAIN_PART}
        
      fi
    
    fi 

  done
}

function check_fqdn_ip {
  local FQDN=$1
  local FQDN_IP=$(dig +short ${FQDN} @${PUBLIC_DNS_SERVER})
  # The following (echo) will be used as return value.
  echo ${FQDN_IP}
}


function do_check_if_fqdn_is_a_domain {

  # Don't print out anything in the function except what you what to print.
  #   Otherwise, this will affect the return values being sent by the last
  #   echo commands.
  
  local FQDN=$1

  ${DO_CLI} compute domain get ${FQDN}  > /dev/null 2>&1

  if [ $? -eq 0 ]; then
    echo "domain"
  else
    echo "host"
  fi
}


function do_install_digitalocean_cli {
  local DNS_PROVIDER=$1

  local CLI_CHECK=$(which ${DO_CLI})

  if [ -z "${CLI_CHECK}" ]; then
    echo "CLI for ${DNS_PROVIDER} does not exist. Downloading and installing ..."
    echo "Installing CLI for ${DNS_PROVIDER} ..."
    # https://docs.digitalocean.com/reference/doctl/how-to/install/
    wget https://github.com/digitalocean/doctl/releases/download/v1.101.0/doctl-1.101.0-linux-amd64.tar.gz
    tar xf doctl-1.101.0-linux-amd64.tar.gz
    mv doctl ${TOOLS_DIRECTORY}/
  else
    echo "CLI for ${DNS_PROVIDER} exists. Good!"
    which ${DO_CLI}
  fi

  
}



#######################################################################
echo

DNS_QUERY='dig TXT +short o-o.myaddr.l.google.com @ns1.google.com'

MY_WAN_IP=$(${DNS_QUERY} | tr -d \")

echo "MY_WAN_IP: ${MY_WAN_IP}"

SCRIPT_PATH=$(realpath $(dirname $0))
echo "Script running from this path: ${SCRIPT_PATH}"

CONFIG_FILE=${SCRIPT_PATH}/config/dns-updater.conf
SECRETS_FILE=${SCRIPT_PATH}/secrets/dns-updater.env

DO_CLI=doctl

# This TOOLS_DIRECTORY needs to be in the path of the user running this script.
#   Also, the user should have write privileges on it.
# The other solution is that you install certain tools yourself,
#   before running this script. Such as dig, doctl, aws cli, etc.

TOOLS_DIRECTORY=/home/kamran/.local/bin

# Google:
PUBLIC_DNS_SERVER=8.8.8.8
# Cloudflare:
# PUBLIC_DNS_SERVER=1.0.0.1
# PUBLIC_DNS_SERVER=1.1.1.1



if [ ! -d ${TOOLS_DIRECTORY} ]; then
  echo "TOOLS_DIRECTORY - ${TOOLS_DIRECTORY} does not exist."
  echo "Various CLI files cannot be downloaded. Please fix this."
  echo "The directory needs to be in the PATH of the user running this script, with write permissions."
fi




if [ -r ${SECRETS_FILE} ]; then
  echo "Reading secrets file - ${SECRETS_FILE}"
  source ${SECRETS_FILE}
fi
  
echo

TMP_CONFIG_FILE=$(mktemp)

echo "Reading config file: ${CONFIG_FILE}"

grep -E -v "^#|^$" ${CONFIG_FILE} > ${TMP_CONFIG_FILE}


while IFS= read -r LINE; do
  echo "Processing line - ${LINE}"
  DNS_PROVIDER=$(echo ${LINE} | cut -f1 -d\:)
  DNS_FQDNS=$(echo ${LINE} | cut -f2 -d\:)
  case ${DNS_PROVIDER} in
    digitalocean)
      echo "Using DNS Provider - Digital Ocean ..."
      do_install_digitalocean_cli ${DNS_PROVIDER}
      do_update_digitalocean ${DNS_FQDNS}
      ;;
    aws)
      ;;
    *)
    echo "This provider is not available yet."
    ;;
  
  esac
done < ${TMP_CONFIG_FILE}



################ START - Functions definitions: ########################



